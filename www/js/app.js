// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var app = angular.module('cidadeMaisSegura', ['ionic']);

app.run(function($ionicPlatform, $rootScope, $window) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });

  $rootScope.toUrl = function(url) {
    if ( url != "#")
        $window.location =  url;
  };

});


app.config(function($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise('/');

  $stateProvider.state('login', {
    url: '/',
    templateUrl: 'views/login.html',
    controller: 'Login',
    cache: false
  }); 

  $stateProvider.state('feed', {
    url: '/feed',
    templateUrl: 'views/feed.html',
    controller: 'Feed',
    cache: false
  });

  $stateProvider.state('show', {
    url: '/show',
    templateUrl: 'views/show.html',
    controller: 'Show',
    cache: false
  });

  $stateProvider.state('Termos', {
    url: '/termos',
    templateUrl: 'views/termos.html',
    controller: 'Termos',
    cache: false
  });

  $stateProvider.state('Sobre', {
    url: '/sobre',
    templateUrl: 'views/sobre.html',
    controller: 'Sobre',
    cache: false
  });


});


app.controller("contentController", function($scope, $ionicSideMenuDelegate, $window) {

  $scope.toggleLeft = function() {
    $ionicSideMenuDelegate.toggleLeft();
  };

  $scope.changeLocation = function(url) {
    if ( url != "#")
      $window.location =  url;

    $ionicSideMenuDelegate.toggleLeft();
  };


});


app.controller("Login", function($scope, $rootScope) {
  $rootScope.title = null;
});

app.controller("Feed", function($scope, $rootScope) {
  $rootScope.title = "Feed de solicitações";
});

app.controller("Termos", function($scope, $rootScope) {
  $rootScope.title = "Termos de uso";
});

app.controller("Sobre", function($scope, $rootScope) {
  $rootScope.title = "Sobre";
});

app.controller("Show", function($scope, $rootScope) {
  $rootScope.title = "Visualizar solicitação";

  window.initialize =  function initialize() {
    console.log("fui chamado");
    var mapOptions = {
      zoom: 14,
      center: new google.maps.LatLng(-34.397, 150.644)
    };

    var map = new google.maps.Map(document.getElementById('map-canvas'),
        mapOptions);
  };

  function loadScript() {
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp' +
        '&signed_in=true&callback=initialize';
    document.body.appendChild(script);
  }

  loadScript();

});



